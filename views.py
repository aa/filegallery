from django.shortcuts import get_object_or_404, render
from django.urls import reverse
from django.http import HttpResponse, HttpResponseRedirect
from .models import Folder, File, strip_media_extension
from itertools import groupby
from operator import itemgetter
import re


def index (request):
    folders = Folder.objects.filter(parent__isnull=True)
    return render(request, "files/index.html", { 'folders': folders })

def folder (request, folder_id):
    folder = Folder.objects.get(id=folder_id)
    # group by known media extensions
    def decorate (file):
        base, ext = strip_media_extension(file.name)
        return {
            'base': base,
            'ext': ext,
            'file': file
        }
    getbase = itemgetter("base")
    gg = [decorate(x) for x in folder.files.all()]
    gg.sort(key=getbase)
    gg = groupby(gg, key=getbase)
    gg = [(x, list(y)) for x, y in gg]
    return render(request, "files/folder.html", { 'folder': folder, 'gg': gg })

def file (request, file_id):
    file = File.objects.get(id=file_id)
    return render(request, "files/file.html", { 'file': file })

def folder_edit (request, folder_id):
    folder = get_object_or_404(Folder, pk=folder_id)
    folder.description = request.POST.get('description', '').strip()
    folder.save()
    return HttpResponseRedirect(reverse('files:folder', args=(folder.id,)))

def media_type (files):
    video = False
    audio = False
    for f in files:
        if f.content_type.startswith("video/"):
            video = True
        elif f.content_type.startswith("audio/"):
            audio = True
    if video:
        return "video"
    elif audio:
        return "audio"

def path (request, slug, path=None):
    f = get_object_or_404(Folder, local_slug=slug)

    if path:
        for p in path.split("/"):
            try:
                f = f.subfolders.get(name=p)
            except Folder.DoesNotExist:
                try:
                    f = f.files.get(name=p)
                except File.DoesNotExist:
                    # VIRTUAL HTML ?
                    if p.endswith(".html"):
                        p = p[:-5]
                        vitems = list(f.files.filter(name__startswith=p).order_by("name"))
                        return render(request, "files/media.html", {
                            "type": media_type(vitems),
                            "media": vitems,
                            "file": vitems[0],
                            "base": strip_media_extension(vitems[0].name)[0]
                        })
                        return HttpResponse("VHTML {0} {1} {2}".format(p, f.url()))
    if isinstance(f, File):
        return file(request, f.id)
    else:
        return folder(request, f.id)

