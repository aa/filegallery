from django.core.management.base import BaseCommand, CommandError
from django.conf import settings
from files.models import Folder
import os


class Command(BaseCommand):
    help = ''

    def add_arguments(self, parser):
        parser.add_argument('path')
        parser.add_argument('url')

    def handle(self, *args, **options):
        url = options['url']
        path = options['path'].rstrip('/') + "/"
        f = Folder(local_url=url, local_path=path)
        f.changed()
        f.save()

