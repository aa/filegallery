from django.core.management.base import BaseCommand, CommandError
from django.conf import settings
from files.models import Folder
import os


class Command(BaseCommand):
    help = 'sync files'

    def add_arguments(self, parser):
        parser.add_argument('path')
        parser.add_argument('--maxdepth', type=int, default=5)

    def handle(self, *args, **options):
        include = None
        if hasattr(settings, 'FILES_INCLUDE'):
            include = settings.FILES_INCLUDE

        # print ("INCLUDE", include, file=self.stderr)
        path = options['path']
        try:
            folder = Folder.objects.get(local_path=path)
            folder.walk(cmd=self, maxDepth=options['maxdepth'], include=include)
        except Folder.DoesNotExist:
            raise CommandError('Folder with path "%s" does not exist' % path)

        # self.stdout.write(self.style.SUCCESS('Successfully closed poll "%s"' % poll_id))
