from django.core.management.base import BaseCommand, CommandError
from django.conf import settings
from files.models import Folder, File
import os, json
from urllib.request import urlopen
from urllib.error import HTTPError
from urllib.parse import urlparse

import re
from itertools import tee

# def pairwise(iterable):
#     "s -> (s0,s1), (s1,s2), (s2, s3), ..."
#     a, b = tee(iterable)
#     next(b, None)
#     return izip(a, b)
def pairwise(iterable):
    "s -> (s0, s1), (s2, s3), (s4, s5), ..."
    a = iter(iterable)
    return zip(a, a)

def processmulti (text):
    m = re.search(r"<multi>(.*)</multi>", text)
    if m:
        contents = m.group(1)
        ret = {}
        for lang, text in pairwise(re.split("\[(\w+)\]", contents)[1:]):
            # print lang, text
            ret[lang] = text
        if 'en' not in ret:
            ret['en'] = text
        if 'nl' not in ret:
            ret['nl'] = text
        if 'fr' not in ret:
            ret['fr'] = text
        return ret
    else:
        return {'en': text, 'fr': text, 'nl': text}


def index_urls (url):
    yield url + "index.json"
    parts = urlparse(url)
    if parts.netloc == "sound.constantvzw.org":
        yield "http://media.constantvzw.org/s" + parts.path + "index.json"

def process_description (text):
    multi = processmulti(text)
    if multi:
        text = multi.get("en", multi.get("fr", multi.get("nl")))
    return text.strip()

class Command(BaseCommand):
    help = 'Attempt to process descriptions from index.json files'

    def add_arguments(self, parser):
        parser.add_argument('--limit', type=int, default=None)

    def handle(self, *args, **options):
        limit = options.get("limit")
        c = 0
        for r in Folder.objects.all():
            self.stdout.write("{0}".format(r.url()))
            for indexurl in index_urls(r.url()):
                # indexurl = r.url + "index.json"
                try:
                    f = urlopen(indexurl)
                    # self.stdout.write("HTTP RESPONSE {0}".format(f.getcode()))
                    print ("read {0}".format(indexurl), file=self.stdout)
                    d = json.loads(f.read().decode("utf-8"))
                    description = process_description(d.get("description", ""))
                    if description:
                        print("Setting description of folder {0} to {1}".format(r.url(), description), file=self.stdout)
                        r.description = description
                        r.save()
                        # r.process_description()
                        c += 1
                        if limit != None and c>=limit:
                            break
                    if 'children' in d:
                        for cnode in d['children']:
                            cd = process_description(cnode.get("description", ""))
                            if cd:
                                cobj = File.objects.get(parent=r, name=cnode.get('name'))
                                cobj.description = cd
                                print("Setting description of file {0} to {1}".format(cobj.url(), cd), file=self.stdout)
                                cobj.save()

                except HTTPError as e:
                    # elf.stdout.write("error: {0}".format(e))
                    pass






