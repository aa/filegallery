Visualizing / rewriting file system
* synching a "minimal" database layer to reflect changes in the actual filesystem
* hashing file contents for "content-based" indexing
* Hooks for editing content including:
    * renaming, moving
    * uploading
    * deleting
* (evt) support for versioning / git
* File grouping based on
    * derivative versions (transcodes, thumbnails)
* Inter-file relationships
    * HTML links
    * makefile described dependencies


Interesting features

* printable hard disk indexes
* cross-searchable (js) indexes

Flip / flip with filesystem integration or not ...
* Would be nice as "external" system


Practical
* Shift to be truly filebased (NO HTTP!) .. URLs secondarily


TODO
=====

x Group files by known media extensions
x File-like URLs
x Virtual Player view (.html)
* Install!
* Authentication
* Editable item titles/descriptions (on the item page ?!) .. Maybe a separate edit view ?!
* Language ?!
* RSS feed
* Sync to files ... Upload / Folder creation via ssh

* ? Project based views ?! (how would that work ... needs feedback from index?)


