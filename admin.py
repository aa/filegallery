from django.contrib import admin
from .models import Folder, File

# class ResourceAdmin(admin.ModelAdmin):
#     search_fields = ("url", )
#     list_display = ("url", "update", 'content_type', 'content_length', 'width', 'height', 'duration', 'pages', 'date')
#     list_editable = ("update", )
#     list_filter = ['content_type', 'date']
#     date_hierarchy = "date"
#     raw_id_fields = ("version_of","thumbnail_for", "is_described_by")

class FolderAdmin (admin.ModelAdmin):
    search_fields = ("name", "local_path" )
    list_display = ("path", "last_modified")
    # list_editable = ("update", )
    # date_hierarchy = "date"
    # raw_id_fields = ("version_of","thumbnail_for", "is_described_by")

class FileAdmin (admin.ModelAdmin):
    search_fields = ("name", )
    list_display = ("path", "last_modified", "content_type")
    list_filter = ['content_type', 'last_modified']
    date_hierarchy = "last_modified"

admin.site.register(Folder, FolderAdmin)
admin.site.register(File, FileAdmin)


