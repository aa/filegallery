from django.db import models
import os, time, datetime
from urllib.parse import urlparse, quote as urlquote, unquote as urlunquote, urljoin
import magic
from itertools import chain
from django.conf import settings
from index.models import Resource, ResourceLink
from django.urls import reverse
from django.contrib.contenttypes.fields import GenericRelation
import re

# SIGNALS
from django.db.models.signals import pre_save
from django.dispatch import receiver
import html5lib


MEDIA_EXT_PATTERN = re.compile(r"\.(jpg|jpeg|png|svg|gif|mkv|wav|mpeg|mp3|mp4|aac|ogg|ogv|webm)$", re.I)
def strip_media_extension (x):
    m = MEDIA_EXT_PATTERN.search(x)
    if m:
        start = m.start()
        base = x[:start]
        ext = x[start:]
        return (base, ext)
    return (x, None)

def timestamp_to_dt (ts):
    ret = datetime.datetime.fromtimestamp(ts)
    return settings.LOCAL_TZ.localize(ret)

def listdir_r (path):
    """
    Only list items that are readable: Created to hide/skip non-readable directories that fail when entered
    """
    items = os.listdir(path)
    return [x for x in items if os.access(os.path.join(path, x), os.R_OK)]

def datetime_totimestamp(dt):
    """ only accurate to whole number """
    return int(time.mktime(dt.timetuple()))


def decr (x):
    if x == None:
        return None
    return x-1

from random import randint
def default_slug ():
    return chr(97+randint(0, 26))

class Folder (models.Model):
    name = models.CharField(max_length=200, blank=True)
    parent = models.ForeignKey('Folder', on_delete=models.CASCADE, related_name="subfolders", blank=True, null=True)
    mtime = models.IntegerField()
    last_modified = models.DateTimeField()
    links = GenericRelation(ResourceLink)

    local_slug = models.SlugField(max_length=10, blank=True)
    local_path = models.CharField(max_length=200, blank=True)
    local_url = models.URLField(blank=True)

    description = models.TextField (blank=True)



    def title (self):
        return self.name

    def path (self):
        if self.local_path:
            return self.local_path
        else:
            return os.path.join(self.parent.path(), self.name)

    def slug (self):
        if self.local_slug:
            return self.local_slug
        else:
            return self.parent.slug()

    def relpath (self):
        if self.local_path:
            return ''
        else:
            return os.path.join(self.parent.relpath(), self.name)

    def url (self):
        if self.local_url:
            return self.local_url
        else:
            return urljoin(self.parent.url(), urlquote(self.name)+"/")

    def changed (self):
        stat = os.lstat(self.path())
        mtime = stat.st_mtime
        # if self.mtime == None or int(mtime) != datetime_totimestamp(self.last_modified):
        if self.mtime == None or mtime != self.mtime:
            self.mtime = mtime
            self.last_modified = timestamp_to_dt(mtime)
            return True

    def default_url (self):
        if self.local_slug != None and self.local_slug != '':
            return reverse("files:path", args=(self.local_slug,))
        else:
            return reverse("files:path", args=(self.slug(), self.relpath()))
        # return reverse("files:folder", args=(self.id,))


    def walk (self, entering=False, maxDepth=None, exclude=None, include=None, force=False, all=False, cmd=None):
        """
        Assumes resource + path is a local directory
        """
        path = self.path()
        if cmd:
            print ("[walk] {0} ({1})".format(path, maxDepth), file=cmd.stdout)
        changed = False

        # lookup existing listed children in the database
        oldchildrenbykey = {x.name: x for x in chain(Folder.objects.filter(parent=self), File.objects.filter(parent=self))}
        # oldchildrenbykey = {x.filename: x for x in Resource.objects.filter(parent=self)}

        for child_filename in sorted(listdir_r(path)):
            # sanity check for utf-8 complicance ...
            try:
                child_filename_bytes = child_filename.encode("utf-8")
                child_filename = child_filename_bytes.decode("utf-8")
            except UnicodeEncodeError:
                if cmd:
                    print ("UNICODE ERROR, SKIPPING FILE", file=cmd.stderr)
                continue
            childpath = os.path.join(path, child_filename)
            # childurl = urljoin(self.url, urlquote(child_filename))

            ### CHECK EXCLUDE LIST
            xclude = False
            # exclude dot files unless --all is specified
            if not all and child_filename.startswith(".") and (include == None or child_filename not in include):
                xclude = True
            elif exclude != None:
                # Honor any exclude patterns
                for x in exclude:
                    if fnmatch(child_filename, x):
                        xclude = True
                        break

            if xclude:
                # if cmd:
                #     print ("Excluding {0}".format(childpath), file=cmd.stderr)
                continue

            if child_filename in oldchildrenbykey:
                oldchild = oldchildrenbykey[child_filename]
                # remove from the dictionary (remaining keys are considered deleted files)
                del oldchildrenbykey[child_filename]

                if os.path.isfile(childpath):
                    if force or oldchild.changed():
                        # print ("Mf", childpath)
                        oldchild.update = True
                        # apply_hook_args(update, childpath, oldchild, oldchild['mimetype'], xpansionvars(childpath, thenodepath, basepath, indexpath))
                        # apply_hook_args(modify, childpath, oldchild, oldchild['mimetype'], xpansionvars(childpath, thenodepath, basepath, indexpath))
                        changed=True
                else:
                    # SUBDIRECTORY... recurse
                    if maxDepth == None or maxDepth >= 1:
                        if oldchild.walk(exclude=exclude, include=include, maxDepth=decr(maxDepth), force=force, all=all, cmd=cmd):
                            changed = True
                    else:
                        if cmd:
                            print ("Excluding {0} due to maxDepth".format(child_filename), file=cmd.stderr)

            else:
                if os.path.isfile(childpath):
                    newnode = File(parent=self, name=child_filename)
                    newnode.changed()
                    newnode.save()
                    # print ("Nf", childpath)
                    # apply_hook_args(create, childpath, newnode, newnode['mimetype'], xpansionvars(childpath, thenodepath, basepath, indexpath))
                    # apply_hook_args(update, childpath, newnode, newnode['mimetype'], xpansionvars(childpath, thenodepath, basepath, indexpath))
                else:
                    # newnode['is_directory'] = True
                    # child_filename = child_filename.rstrip("/") + "/"
                    newnode = Folder(parent=self, name=child_filename)
                    newnode.changed()
                    newnode.save()
                    if maxDepth == None or maxDepth >= 1:
                        newnode.walk(entering=True, maxDepth=decr(maxDepth), exclude=exclude, include=include, force=force, all=all, cmd=cmd)
                    else:
                        if cmd:
                            print ("NOT exploring {0} due to maxDepth restriction".format(child_filename), file=cmd.stderr)
                    # print ("Nd", childpath)
                    # apply_hook_args(create, childpath, newnode, newnode['mimetype'], xpansionvars(childpath, nodepath(childpath, jsonpath, basepath, indexpath), basepath, indexpath))
                    # apply_hook_args(update, childpath, newnode, newnode['mimetype'], xpansionvars(childpath, nodepath(childpath, jsonpath, basepath, indexpath), basepath, indexpath))

                # implicit via parent=self
                # node['children'].append(newnode)
                changed = True

        for oname, oldnode in oldchildrenbykey.items():
            # childpath = os.path.join(path, oname)
            # node['children'].remove(oldnode)
            oldnode.delete()
            if cmd:
                print ("Deleting {0}".format(childpath), file=cmd.stdout)
            # if 'mtime' in oldnode:
            #     print ("Df", os.path.join(path, oname), file=sys.stderr)
            #     # apply_hook("Df", os.path.join(path, oname), None, hook)
            #     # apply(hook, "Df", os.path.join(path, oname), None)
            #     # apply_hook_args (childpath, None, oldnode['mimetype'], args['delete'], xpansionvars(childpath, thenodepath, basepath, indexpath))
            # else:
            #     print ("Dd", os.path.join(path, oname), file=sys.stderr)
            #     # apply_hook("Dd", os.path.join(path, oname), None, hook)
            #     # apply(hook, "Dd", os.path.join(path, oname), None)
            # apply_hook_args (delete, childpath, None, oldnode['mimetype'], xpansionvars(childpath, thenodepath, basepath, indexpath))
            changed = True

        # if a folder contents changes, I should report this folder as updated
        # if changed and not entering: # when entering is true, directory would already have been reported as new
        #     pass
            # print ("Md", path)   
            # apply_hook("Md", path, None, hook)
            # apply(hook, "Md", path, None)
            # apply_hook_args (update, path, None, "directory/directory", xpansionvars(path, thenodepath, basepath, indexpath))
            # apply_hook_args (modify, path, None, "directory/directory", xpansionvars(path, thenodepath, basepath, indexpath))

        return changed

import markdown as md 

@receiver(pre_save, sender=Folder)
def folder_process_description (sender, instance, **kwargs):
    # clear existing links
    folder = instance
    folder.links.all().delete()
    src = md.markdown(folder.description)
    t = html5lib.parseFragment(src, namespaceHTMLElements=False)
    for link in t.findall('.//a[@href]'):
        href = link.attrib.get('href')
        r, _ = Resource.ensure_resource_for(href)
        rlink = ResourceLink(resource=r, content_object=folder)
        rlink.save()

def ensure_trailing_slash (x):
    return x.rstrip("/")+"/"

class File (models.Model):
    name = models.CharField(max_length=200, blank=False)
    parent = models.ForeignKey(Folder, on_delete=models.CASCADE, related_name="files")
    mtime = models.IntegerField()
    content_length = models.IntegerField()
    links = GenericRelation(ResourceLink)

    extension = models.CharField(max_length=64, blank=True)
    last_modified = models.DateTimeField()
    content_type = models.CharField(max_length=200, blank=True)
    derived_from = models.ForeignKey('File', on_delete=models.CASCADE, null=True, blank=True)
    sha1 = models.CharField(max_length=200, blank=True)

    description = models.TextField (blank=True)

    def get_thumb (self):
        if self.parent:
            try:
                return self.parent \
                    .subfolders.get(name=".thumbs") \
                    .files.get(name=self.name+".1.jpg")
            except File.DoesNotExist:
                return

    def path(self):
        return os.path.join(self.parent.path(), self.name)

    def slug (self):
        return self.parent.slug()

    def relpath (self):
        return os.path.join(self.parent.relpath(), self.name)

    def base (self):
        return strip_media_extension(self.name)[0]

    def relbase (self):
        return os.path.join(self.parent.relpath(), self.base()+".html")

    def url (self):
        return urljoin(ensure_trailing_slash(self.parent.url()), urlquote(self.name))

    def changed (self):
        path = self.path()
        stat = os.lstat(path)
        mtime = stat.st_mtime
        # if self.mtime == None or int(mtime) != datetime_totimestamp(self.last_modified):
        if self.mtime == None or mtime != self.mtime:
            self.mtime = mtime
            self.last_modified = timestamp_to_dt(mtime) # UPDATE mtime ... but has the contents changed?
            self.content_length = stat.st_size
            self.content_type = magic.from_file(path, mime=True)
            self.save()
            return True

            # OLD HASH CALCULATION... (for later ;)
            # if cmd:
            #     cmd.stdout.write("Calculating sha1 of {0}".format(path))
            # sha1 = githash(path)
            # if self.sha1 == None or sha1 != self.sha1:
            #     self.sha1 = sha1
            #     self.content_length = stat.st_size
            #     self.content_type = magic.from_file(path, mime=True)
            #     self.save()
            #     return True
            # else:
            #     self.save()
            #     return False



    # def parent_path (self):
    #     if self.parent:
    #         return self.parent.parent_path() + [self]
    #     else:
    #         return [self]

    # def breadcrumbs (self):
    #     if self.parent:
    #         return self.parent.parent_path()
    #     else:
    #         return []

    # @property
    # def filename(self):
    #     if self.url.endswith("/"):
    #         url = self.url[:-1]
    #     else:
    #         url = self.url
    #     return urlunquote(os.path.basename(urlparse(url).path))

    # @property
    # def path(self):
    #     for local in Local.objects.all():
    #         if self.url.startswith(local.url):
    #             return local.url_to_path(self.url)



    # def sync (self, cmd=None, include=None, maxdepth=None):
    #     self.walk(self.path, cmd=cmd, include=include, maxDepth=maxdepth)


# class Local (models.Model):
#     """ Locals map local paths to urls """
#     path = models.CharField(max_length=1000)
#     url = models.URLField()
#     folder = models.OneToOneField(Folder, on_delete=models.CASCADE, related_name="local", null=True, blank=True)

    # def url_to_path (self, url):
    #     selfpath = urlparse(self.url).path
    #     respath = urlparse(url).path
    #     assert (respath.startswith(selfpath))
    #     rpath = urlunquote(os.path.relpath(respath, selfpath))
    #     return os.path.join(self.path, rpath)

    # def path_to_url (self, path):
    #     assert (path.startswith(self.path))
    #     rpath = urlquote(os.path.relpath(path, self.path))
    #     return os.path.join(self.url, rpath)

    # @classmethod
    # def get_url_for_path (cls, path):
    #     for local in Local.objects.all():
    #         if path.startswith(local.path):
    #             return local.path_to_url(path)

    # @classmethod
    # def get_path_for_url (cls, url):
    #     for local in Local.objects.all():
    #         if url.startswith(local.url):
    #             return local.url_to_path(url)