from django.urls import path

from . import views

app_name = 'files'
urlpatterns = [
    path('', views.index, name='filesindex'),
    path('folder/<int:folder_id>/', views.folder, name='folder'),
    path('folder/<int:folder_id>/edit/', views.folder_edit, name='folder_edit'),
    path('file/<int:file_id>/', views.file, name='file'),
    path('<slug:slug>/', views.path, name="path"),
    path('<slug:slug>/<path:path>', views.path, name="path"),
    path('<slug:slug>/<path:path>.html', views.path, name="filewrapper"),
    path('<slug:slug>/<path:ppath>/<path:path>.html', views.path, name="filewrapper")
]
